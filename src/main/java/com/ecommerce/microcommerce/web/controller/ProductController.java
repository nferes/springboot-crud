package com.ecommerce.microcommerce.web.controller;


import com.ecommerce.microcommerce.model.Product;
import com.ecommerce.microcommerce.web.dao.ProductDao;
import com.ecommerce.microcommerce.web.exceptions.NoProductException;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Objects;

@RestController
public class ProductController {
    private final ProductDao productDao;

    public ProductController(ProductDao productDao){
        this.productDao = productDao;
    }

    @GetMapping("/Products")
    public MappingJacksonValue ProductsList(){
        List<Product> products = productDao.findAll();
        SimpleBeanPropertyFilter myFilter = SimpleBeanPropertyFilter.serializeAllExcept("sellingPrice");
        FilterProvider ourFiltersList = new SimpleFilterProvider().addFilter("myDynamicFilter", myFilter);
        MappingJacksonValue filtersProducts = new MappingJacksonValue(products);
        filtersProducts.setFilters(ourFiltersList);
        return filtersProducts;
    }

    @GetMapping("/Products/{id}")
    public Product showProduct(@PathVariable int id){

        Product product= productDao.findById(id);
        if(product==null) throw new NoProductException("The product with the id "+id+" does not exist");
        return product;
    }

    @GetMapping("test/Products/{priceLimit}")
    public List<Product> requestTest(@PathVariable int priceLimit){

        return productDao.findByPriceGreaterThan(priceLimit);
    }

    @PostMapping("/Products")
    public ResponseEntity<Product> addProduct(@Valid @RequestBody Product product){
        Product productAdded = productDao.save(product);
        if(Objects.isNull(productAdded)) {
            return ResponseEntity.noContent().build();
        }
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(productAdded.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping ("/Products/{id}")
    public void deleteProduct(@PathVariable int id){
        productDao.deleteById(id);
    }
    @PutMapping ("/Products")
    public void updateProduct(@RequestBody Product product){
        productDao.save(product);
    }
}
