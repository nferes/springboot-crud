package com.ecommerce.microcommerce.model;


import com.fasterxml.jackson.annotation.JsonFilter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

//@JsonFilter("myDynamicFilter")
@Entity
public class Product {
    @Id
    private int id;
    @Size(min=3, max=25)
    private String name;
    @Min(value=1)
    private int price;



    private int sellingPrice;
    public Product() {
    }

    public Product(int id, String name, int price, int sellingPrice) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.sellingPrice = sellingPrice;
    }

    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id = id;    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;    }

    public int getPrice(){
        return price;
    }
    public void setPrice(int price){
        this.price = price;    }

    public int getSellingPrice(){
        return sellingPrice;
    }
    public void setSellingPrice(int sellingPrice){
        this.sellingPrice = sellingPrice;    }

    @Override
    public String toString(){
        return "rProduct{"+
                "id="+id+
                ", name='"+name+'\''+
                ", price=" + price+
                '}';
    }
}
